# Tic Tac Toe game in Ruby!

Just a simple console-based implementation of the famous Tic-Tac-Toe game.

## How to play
$ gem install bundler

...


$ bundle

...

$ irb .Ilib

irb(main):001:0> require 'game'; Game.start()

Welcome to Tic Tac Toe!

0       => Play against AI

1       => Play against Human


...just follow the instructions and enjoy!