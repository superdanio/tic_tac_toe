require 'strategies/best_next_strategy'
require 'state'

describe BestNextStrategy do
  
  context "with a state with one imminent loser" do
    it "should choose the state to remove the loser" do
      state = State.new( [ [ 'O', ' ', 'O' ], [ ' ', 'X', ' ' ], [ 'X', 'O', ' ' ], ] )
      expect(BestNextStrategy.new('X').next_move(state)).to eq([ [ 'O', 'X', 'O' ], [ ' ', 'X', ' ' ], [ 'X', 'O', ' ' ], ])
    end
  end
  
  context "with a state with one imminent winner" do
    it "should choose the state to win" do
      state = State.new( [ [ 'O', ' ', 'O' ], [ ' ', 'X', ' ' ], [ 'O', 'X', ' ' ], ] )
      expect(BestNextStrategy.new('X').next_move(state)).to eq([ [ 'O', 'X', 'O' ], [ ' ', 'X', ' ' ], [ 'O', 'X', ' ' ], ])
    end
  end
  
end