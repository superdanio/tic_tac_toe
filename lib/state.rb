class State < Array
  
  def initialize(state)
    super(state)
  end
  
  def deep_copy
    State.new(self.inject([]) {|res, x| res + [x.inject([]) { |res2, x2| res2 + [x2.dup]}]}) 
  end
  
  def apply(row, col, player)
    new_state = self.deep_copy
    new_state[row][col] = player
    new_state
  end
  
  def pretty_print
    (0..2).inject('') { |res,x| res + prepare_line(x)}
  end
  
  private
  
  def prepare_line(index)
    if index == 0
      res = "      0     1     3   \n "
      res += "  |‾‾‾‾‾|‾‾‾‾‾|‾‾‾‾‾|\n "
    else
      res = "   |     |     |     |\n "
    end
    res += index.to_s + " |  %s  |  %s  |  %s  |\n" % self[index]
    res + "   |_____|_____|_____|\n"
  end
  
end
