require 'board'

class HumanGame
  
  def initialize
    @board = Board.new
  end
  
  def start
    puts "Welcome to Human vs Human challenge!"
    until @board.game_over?
      available_moves = @board.available_moves
      move = available_moves[get_move_from_player(available_moves)]
      @board.update(@board.current.apply(move[0], move[1], @board.current_player))
    end
    puts @board
    if @board.draw? then puts "Game over. It's a Draw!"
    else puts "Game over. Player '%s' wins" % @board.winner end
  end
  
  private
  
  def get_move_from_player(moves)
    begin
      puts @board
      puts "Player '%s', place your move!" % @board.current_player
      puts moves.each_with_index.map {|move, i| "%d\t=> %s" % [i, move]}.join("\n")
      answer = gets.strip
    end until proper_answer?(answer, moves)
    answer.to_i
  end
  
  def proper_answer?(answer, moves)
    answer =~ /\A\d+\Z/ && (0..moves.length-1).include?(answer.to_i)
  end
  
end
