require 'ai_game'
require 'human_game'

module Game
  
  @game_types = [AIGame, HumanGame]
  
  def self.start
    puts "Welcome to Tic Tac Toe!"
    begin
      game_type = get_game_type
      @game_types[game_type].new.start
      puts "Do you want to play again?"
      answer = gets.strip
    end while answer =~ /\A(y|yes)\z/i
  end
  
  private 
  
  def self.get_game_type
    begin
      puts "0\t=> Play against AI"
      puts "1\t=> Play against Human"
      answer = gets.strip
    end until answer =~ /\A(0|1)\z/
    answer.to_i
  end
  
end
