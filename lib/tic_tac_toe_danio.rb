require 'game'
require 'state_space'
require 'strategies/best_strategy'
require 'strategies/best_next_strategy'
require 'strategies/random_next_strategy'