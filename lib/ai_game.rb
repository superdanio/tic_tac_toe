require 'board'
require 'ai_strategies'

class AIGame
  
  def initialize
    @board = Board.new
    @ai_player = StateSpace.switch_player(@board.current_player)
  end
  
  def start
    puts "Welcome to Human vs CPU challenge!"
    ai_strategy = get_ai_strategy()
    until @board.game_over?
      if @board.current_player.eql?(@ai_player)
        puts @board
        puts "CPU's turn."
        print "Thinking..."
        thinking(1.5)
        @board.update(ai_strategy.next_move(@board.current))
      else
        available_moves = @board.available_moves
        move = available_moves[get_move_from_player(available_moves)]
        @board.update(@board.current.apply(move[0], move[1], @board.current_player))
      end
    end
    puts @board
    if @board.draw? then puts "Game over. It's a Draw!"
    else puts("Game over. %s!" % [@board.winner.eql?(@ai_player)?'CPU wins':'You win']) end
  end
  
  private
  
  def get_ai_strategy
    begin
      puts "Select CPU level:"
      puts "0\t=> Easy"
      puts "1\t=> Medium"
      puts "2\t=> Hard"
      answer = gets.strip
    end until answer =~ /\A(0|1|2)\z/
    AIStrategies.strategy(answer.to_i, @ai_player)
  end
  
  def get_move_from_player(moves)
    begin
      puts @board
      puts "Your turn. Place your move!" % @board.current_player
      puts moves.each_with_index.map {|move, i| "%d\t=> %s" % [i, move]}.join("\n")
      answer = gets.strip
    end until proper_answer?(answer, moves)
    answer.to_i
  end
  
  def proper_answer?(answer, moves)
    answer =~ /\A\d+\Z/ && (0..moves.length-1).include?(answer.to_i)
  end
  
  def thinking(seconds, fps=10)
    chars = %w[| / - \\]
    delay = 1.0/fps
    (seconds*fps).round.times{ |i|
      print chars[i % chars.length]
      sleep delay
      print "\b"
    }
    print "\n"
  end
  
end
