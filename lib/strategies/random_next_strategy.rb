require 'strategies/strategy'
require 'state_space'

class RandomNextStrategy < Strategy
  
  def initialize(player)
    @full_space = StateSpace.all_space_for(StateSpace.switch_player(player))
  end
  
  def next_move(state)
    @full_space[state].sample
  end
  
end
