require 'strategies/strategy'
require 'state_space'

class BestStrategy < Strategy
  
  def initialize(player)
    @player = player
    @other_player = StateSpace.switch_player(player)
    @full_space = StateSpace.all_space_for(@other_player)
  end
  
  def next_move(state)
    moves = @full_space[state]
    winners = moves.select {|x| winning_move?(x, @player)}
    non_losers = moves.select {|x| non_losing_move?(x, @player)}
    non_immediate_losers = @full_space[state].reject {|x| immediate_loser(state)}
    return winners.sample unless winners.empty? # try win first
    return non_losers.sample unless non_losers.empty? # try draw
    non_immediate_losers.empty? ? @full_space[state].sample : non_immediate_losers.sample # :(
  end
  
  private
  
  def immediate_loser(state)
    @full_space[state].any? {|x| StateSpace.goal_for?(x, @other_player)}
  end
  
  def winning_move?(state, player)
    StateSpace.goal_for?(state, player) || (non_losing_move?(state, player) && !@full_space[state].empty? && @full_space[state].all? {|x| @full_space[x].any? {|y| winning_move?(y ,player)}})
  end
  
  def losing_move?(state, player)
    @full_space[state].any? {|x| winning_move?(x, StateSpace.switch_player(player))}
  end
  
  def non_losing_move?(state, player)
    @full_space[state].all? { |x| !StateSpace.goal_for?(x, StateSpace.switch_player(player)) && (@full_space[x].empty? || @full_space[x].any?{|y| non_losing_move?(y,player)}) }
  end
  
end
