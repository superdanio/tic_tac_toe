require 'strategies/strategy'
require 'state_space'

class BestNextStrategy < Strategy
  
  def initialize(player)
    @player = player
    @other_player = StateSpace.switch_player(player)
    @full_space = StateSpace.all_space_for(@other_player)
  end
  
  def next_move(state)
    winners = moves = @full_space[state].select {|x| StateSpace.goal_for?(x, @player)}
    return winners.sample unless winners.empty?
    losers = @full_space[state].count {|x| StateSpace.goal_for?(x, @other_player)}
    tmp = @full_space[state].select {|x| @full_space[x].count {|y| StateSpace.goal_for?(y, @other_player)} < losers}
    tmp.empty? ? @full_space[state].sample : tmp.sample
  end
  
end
