require 'state'
require 'state_space'

class Board
  
  attr_reader :current_player
  
  def initialize()
    @current_player = StateSpace::Circle
    @score = StateSpace::Start.deep_copy
  end
  
  def available_moves
    StateSpace.available_moves(@score, @current_player)
  end
  
  def update(state)
    raise 'Move not allowed!' unless StateSpace.next_state(@score, @current_player).include?(state)
    @current_player = StateSpace.switch_player(@current_player)
    @score = state.deep_copy
  end
  
  def current
    @score.deep_copy
  end
  
  def game_over?
    StateSpace.goal?(@score)
  end
  
  def winner
    completed = StateSpace.goal?(@score)
    completed == true ? nil : completed
  end
  
  def draw?
    completed = StateSpace.goal?(@score)
    completed && !StateSpace::Players.include?(completed)
  end
  
  def to_s
    @score.pretty_print
  end
  
end
