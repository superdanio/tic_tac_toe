require 'strategies/random_next_strategy'
require 'strategies/best_next_strategy'
require 'strategies/best_strategy'

module AIStrategies
  
  @strategies = [RandomNextStrategy, BestNextStrategy, BestStrategy]
  
  def self.strategy(level, player)
    @strategies[level].new(player)
  end
  
end