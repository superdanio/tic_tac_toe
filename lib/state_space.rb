require 'state'

module StateSpace
  
  Empty = ' '
  Circle = 'O'
  Cross = 'X'
  Players = [Circle, Cross]
  Start = State.new([ [Empty,Empty,Empty], [Empty,Empty,Empty], [Empty,Empty,Empty] ]).freeze
  
  def self.all_space_for(player)
    populate({}, Start, player)
  end
  
  def self.available_moves(state, player)
    possible_moves = []
    state.each_with_index {|row_val, row_index| row_val.each_with_index {|val, col_index| possible_moves.push([row_index, col_index]) if Empty.eql?(val) }} unless goal?(state)
    possible_moves
  end
  
  def self.next_state(state, player)
    available_moves(state, player).inject([]) do |res, move|
      tmp = state.deep_copy
      tmp[move[0]][move[1]] = player
      res + [tmp]
    end
  end
  
  def self.goal_for?(state, player)
    player.eql?(goal?(state))
  end
  
  def self.goal?(state)
    rows_scored?(state) || columns_scored?(state) || diagonals_scored?(state) || state.all? {|x| x.all? {|y| not Empty.eql?(y)}}
  end
  
  def self.switch_player(player)
    Players[(Players.index(player) + 1) % 2]
  end
  
  private
  
  def self.find(state, states)
    states.each do |key, value|
      return value if key.eql?(state) && value
    end
    return nil
  end
  
  def self.populate(visited, current, player)
    visited[current] = next_state(current, player)
    visited[current].each do |x|
      populate(visited, x, switch_player(player)) unless visited[x]
    end
    visited
  end
  
  def self.rows_scored?(state)
    state.each do |x|
      return x[0] if used?(x[0]) && x.uniq.length == 1
    end
    return nil
  end
  
  def self.columns_scored?(state)
    (0..2).each_with_index do |x,i|
      col = [state[0][i], state[1][i], state[2][i]]
      return col[0] if used?(col[0]) && col.uniq.length == 1
    end
    return nil
  end
  
  def self.diagonals_scored?(state)
    diag1 = [state[0][0], state[1][1], state[2][2]]
    diag2 = [state[0][2], state[1][1], state[2][0]]
    return state[1][1] if used?(state[1][1]) && diag1.uniq.length == 1
    return state[1][1] if used?(state[1][1]) && diag2.uniq.length == 1
    return nil
  end
  
  def self.used?(value)
    !value.eql?(Empty)
  end
  
end
