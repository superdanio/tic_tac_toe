Gem::Specification.new do |s|
  s.name        = 'tictactoe_rb'
  s.version     = '0.0.1'
  s.date        = '2015-05-01'
  s.summary     = "Tic Tac Toe in Ruby!"
  s.description = "A simple implementation of tic tac toe in ruby, providing state spaces and different play strategies."
  s.authors     = ["Daniele Di Federico"]
  s.email       = 'daniele.difederico@gmail.com'
  s.files       = Dir.glob("lib/**/*") + ['README.md']
  s.license     = 'MIT'
  s.add_development_dependency "rspec"
  s.require_path = 'lib'
end